<?php

    class controladorUsuarios {
        

        /*==========================================================
        VERIFICAR SI EL USUARIO EXISTE PARA EL INICIO DE SISION
        ===========================================================*/
        static public function ctrIngresoUsuario() {
            
            if(isset($_POST["ingUsuario"])) {
                
                if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingUsuario"]) && preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingPassword"])){
                    
                    $encriptar = crypt($_POST["ingPassword"], '$2a$07$usesomesillystringforsalt$');

                    $tabla = "usuarios";
                    
                    $item = "usuario";
                    $valor = $_POST["ingUsuario"];
                    
                    $respuesta = modeloUsuarios::mdlMostrarUsuarios($tabla, $item, $valor);
                    
                    if (is_array($respuesta) && $respuesta["usuario"] == $_POST["ingUsuario"] && $respuesta["password"] == $encriptar) {
                        
                        $_SESSION["iniciarSession"] = "ok";
                        $_SESSION["id"] = $respuesta["id"];
                        $_SESSION["nombre"] = $respuesta["nombre"];
                        $_SESSION["usuario"] = $respuesta["usuario"];
                        $_SESSION["foto"] = $respuesta["foto"];
                        $_SESSION["perfil"] = $respuesta["perfil"];
                        
                        echo '<script>
                            
                            window.location = "inicio";

                        </script>';
                        
                    }else {
                        
                        echo '<div class="alert mt-3 alert-danger alert-dismissible fade show">
                                <strong>Error!  Usuario o contraseña incorrecto</strong> 
                              </div>';
                        
                    }
                    
                }
                
            }
            
        }



        /*==========================================================
        REGISTRO DE USUARIOS
        ===========================================================*/
        static public function ctrCrearusuario() {

            if(isset($_POST["nuevoUsuario"])) {

                if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevoNombre"])  && preg_match('/^[a-zA-Z0-9]+$/', $_POST["nuevoUsuario"]) && preg_match('/^[a-zA-Z0-9]+$/', $_POST["nuevoPassword"])){


                    /*==========================================
                    VALIDAR FOTO
                    ============================================*/

                    $ruta = "";

                    if(isset($_FILES["nuevaFoto"]["tmp_name"]) ){

                        list($ancho, $alto) = getimagesize($_FILES["nuevaFoto"]["tmp_name"]);                        

                        $nuevoAncho = 500;
                        $nuevoAlto = 500;

                        /*======================================
                        CREAMOS EL DIRECTORIO DONDE VAMOS
                        A GUARDAR LAS IMAGENES DEL USUARIO
                        ========================================*/

                        $directorio = "vistas/img/usuarios/". $_POST["nuevoUsuario"];

                        mkdir($directorio , 0755);


                        /*==========================================
                        DEACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES 
                        POR DEFECTO DE PHP
                        ============================================*/

                        if($_FILES["nuevaFoto"]["type"] == "image/jpeg"){

                            /*==========================================
                            GUARDAR LA IMAGEN EN EL DIRECTORIO  JPG                           
                            ============================================*/

                            $aleatorio = mt_rand(100,999);

                            $ruta = "vistas/img/usuarios/" . $_POST["nuevoUsuario"] ."/" .$aleatorio . ".jpg";

                            $origen = imagecreatefromjpeg($_FILES["nuevaFoto"]["tmp_name"]);

                            $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

                            imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

                            imagejpeg($destino, $ruta);

                        }

                        if($_FILES["nuevaFoto"]["type"] == "image/png"){

                            /*==========================================
                            GUARDAR LA IMAGEN EN EL DIRECTORIO PNG                           
                            ============================================*/

                            $aleatorio = mt_rand(100,999);

                            $ruta = "vistas/img/usuarios/" . $_POST["nuevoUsuario"] ."/" .$aleatorio . ".png";

                            $origen = imagecreatefrompng($_FILES["nuevaFoto"]["tmp_name"]);

                            $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

                            imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

                            imagepng($destino, $ruta);

                        }
                    }

                    $tabla = "usuarios";

                    $encriptar = crypt($_POST["nuevoPassword"], '$2a$07$usesomesillystringforsalt$');

                    $datos = array("nombre" => $_POST["nuevoNombre"], "usuario" => $_POST["nuevoUsuario"], "password" =>$encriptar, "perfil" => $_POST["perfil"], "foto"=>$ruta);
                    
                    $respuesta = modeloUsuarios::mdlIngresarUsuarios($tabla, $datos);
                    
                    if ($respuesta == "ok"){
                        
                        echo "<script>

                            Swal.fire({
                                icon: 'success',
                                title: 'Excelente',
                                text: 'El usuario " . $_POST['nuevoUsuario'] ." fue ingresado correctamente ',
                            }).then((result)=>{
                            
                                if(result.value) {
                                    window.location = 'usuarios';
                                }

                            });

                        </script>";    
                    }

                }else {

                    echo "<script>

                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Verifique que los campos esten llenos o que no contengan caracteres especiales',
                        }).then((result)=>{
                        
                            if(result.value) {
                                window.location = 'usuarios';
                            }

                        });

                    </script>";    

                }
    

            }

        }
        
    }

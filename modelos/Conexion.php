<?php

    class conexion {
        
        /*=====================================================================
        Conexion de base de datos por PDO
        =======================================================================*/
        static public function conectar () {
            
            $db = new PDO("mysql:host=localhost;dbname=ackucorp", "root", "");
            
            $db->exec("set names utf8");
            
            return $db;
            
        }
        
    }

?>
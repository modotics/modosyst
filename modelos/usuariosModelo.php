<?php

    require_once 'Conexion.php';

    class modeloUsuarios {
        

        /*==========================================================
        LOGIN DE USUARIOS
        ===========================================================*/
        static public function mdlMostrarUsuarios($tabla, $item, $valor) {
            
            $stmt = conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
            $stmt->bindParam(":" . $item, $valor, PDO::PARAM_STR);
            
            $stmt->execute();
            return $stmt-> fetch();

            $stmt->close();
            $stmt = null;
            
        }

        /*==========================================================
        REGISTRO DE USUARIOS
        ===========================================================*/
        static public function mdlIngresarUsuarios($tabla, $datos) {

            $stmt = conexion::conectar()->prepare("INSERT INTO $tabla(nombre, usuario, password, perfil, foto) VALUES(:nombre, :usuario, :password, :perfil, :foto)");
            $stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
            $stmt->bindParam(":usuario", $datos["usuario"], PDO::PARAM_STR);
            $stmt->bindParam(":password", $datos["password"], PDO::PARAM_STR);
            $stmt->bindParam(":perfil", $datos["perfil"], PDO::PARAM_STR);
            $stmt->bindParam(":foto", $datos["foto"], PDO::PARAM_STR);

            if($stmt->execute()){
                return "ok";
            }else{
                return "errror";
            }

            $stmt->close();
            $stmt = null;


        }
        
    }

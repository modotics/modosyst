/*=============================================
SUBIR FOTO DEL USUARIO
=============================================*/

$(".nuevaFoto").change(function(){

    var imagen = this.files[0];
    //console.log("imagen", imagen);

    /*=======================================================
    VALIDAMOS EL FORMATO DE LA IMAGEN QUE NO SEA JPG O PNG
    ========================================================*/

    if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){

        $(".nuevaFoto").val("");

        Swal.fire({
            icon: 'error',
            title: 'Error al subir la imagen',
            text: 'La foto que ingreso debe ser en formato JPG o PNG',            
        })

    }else if (imagen["size"] > 2000000) {

        $(".nuevaFoto").val("");

        Swal.fire({
            icon: 'error',
            title: 'Error al subir la foto',
            text: 'La foto tiene un peso mayor a lo requerido (2 MB)',            
        })

    }else {

        var datosImagen = new FileReader;
        datosImagen.readAsDataURL(imagen);

        $(datosImagen).on("load", function(event){

            var rutaImagen = event.target.result;

            $(".previsualizar").attr("src", rutaImagen);

        })

    }

})
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Administración de usuarios</h1>
                    </div><!-- /.col -->
                  
                  <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                            <li class="breadcrumb-item active">Usuarios Registrados</li>
                        </ol>
                  </div><!-- /.col -->
                  
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Agregar Usuario</button>

            <div class="card mt-5">
                <div class="card-header">
                        <h3 class="card-title">DataTable with default features</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Usuario</th>
                                <th>Foto</th>
                                <th>Perfil</th>
                                <th>Estado</th>
                                <th>Último Login</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>Trident</td>
                                <td>Internet
                                Explorer 4.0
                                </td>
                                <td>Win 95+</td>
                                <td> 4</td>
                                <td>X</td>
                                <td>
                                    <button type="button" class="btn btn-success btn-xs">Activado</button>
                                </td>
                                <td>X</td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></button>
                                        <button type="button" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                                        
                                    </div>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>Trident</td>
                                <td>Internet
                                Explorer 4.0
                                </td>
                                <td>Win 95+</td>
                                <td> 4</td>
                                <td>X</td>
                                <td>
                                    <button type="button" class="btn btn-danger btn-xs">Desactivado</button>
                                </td>
                                <td>X</td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></button>
                                        <button type="button" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                                        
                                    </div>
                                </td>
                            </tr>
                            
                        </tbody>

                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Usuario</th>
                                <th>Foto</th>
                                <th>Perfil</th>
                                <th>Estado</th>
                                <th>Último Login</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            
            
        </div>
        <!-- /.content -->

       <!-- ===============================
                MODAL
        =================================== -->
        <div class="modal" tabindex="-1" role="dialog" id="exampleModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: #455279; color: #ffffff">
                        <h5 class="modal-title">Agregar Usuarios</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        
                        <form method="post" class="mt-3" enctype="multipart/form-data">
                            <div class="col-auto">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-signature"></i></div>
                                    </div>
                                    <input type="text" name="nuevoNombre" class="form-control" placeholder="Nombre" required>
                                </div>
                            </div>

                            <div class="col-auto">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="far fa-user"></i></div>
                                    </div>
                                    <input type="text" name="nuevoUsuario" class="form-control"  placeholder="Usuario" required>
                                </div>
                            </div>

                            <div class="col-auto">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-key"></i></div>
                                    </div>
                                    <input type="password" name="nuevoPassword" class="form-control" placeholder="Contraseña" required>
                                </div>
                            </div>

                            <div class="col-auto">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-users"></i></div>
                                    </div>
                                    <select class="form-control" id="exampleFormControlSelect2" name="perfil" required>
                                        <option value="">Seleccione Perfil</option>
                                        <option>Administrador</option>
                                        <option>Especial</option>
                                        <option>Operador</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group mt-4 pl-2">
                                <span style="margin-bottom: 0">Subir Foto de perfil</span>
                                <input type="file" class="nuevaFoto" name="nuevaFoto">
                            </div>
                            
                            <div class="form-group pl-2">                                
                                <img src="<?php echo $url ?>vistas/img/plantilla/usuario_vacio-min.png" class="img-circle elevation-2 previsualizar" alt="User Image" width="100px">
                                <small style="margin-bottom: 0">Peso máximo de la foto 2 MB</small>
                            </div>
                            
                            
                            <button type="submit" class="btn btn-primary mt-3" style="width: 100%!important; background: #455279!important">Guardar</button>


                            <?php
                            
                                $crearUsuarios = new controladorUsuarios();
                                $crearUsuarios->ctrCrearusuario();
                            
                            ?>
                            
                        </form>

                    </div>
                    
                </div>
            </div>
        </div>


</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>
<!-- /.control-sidebar --> 
<div id="back"></div>
<div class="login-box login-page" style="width: 100%!important; padding: 0 20px 0 20px!important; border: solid 2px #103C81!important;">
    <div class="login-logo mb-5">
        <img src="<?php echo $url ?>vistas/img/logo_inicio.png" class="img-responsive">
    </div>
    <!-- /.login-logo -->
    <div class="card logeo">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Iniciar Sesión</p>

            <form method="post">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Usuario" name="ingUsuario" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="fas fa-user"></i>
                        </div>
                  </div>
                </div>
                
                <div class="input-group mb-3">
                    <input type="password" class="form-control" placeholder="Contraseña" name="ingPassword" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                
                <div class="row">          
                  <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Ingresar</button>
                    </div>
                    <!-- /.col -->
                </div>
                
                <?php
                
                    $login = new controladorUsuarios();
                    $login->ctrIngresoUsuario();
                
                ?>
                
            </form>



            <p class="mb-1 mt-2">
                <a href="forgot-password.html">Recuperar Contraseña</a>
            </p>

        </div>
      <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->


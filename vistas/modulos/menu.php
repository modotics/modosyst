

  <!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?php echo $url ?>vistas/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Acku Corp</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
            if($_SESSION["foto"] == ""){

              echo '<img src="<?php echo $url ?>vistas/dist/img/plantilla/usuario_vacio-min.png" class="img-circle elevation-2" alt="User Image">';

            }else{

              echo '<img src="' . $_SESSION["foto"] . '" class="img-circle elevation-2" alt="User Image">';

            }
          ?>
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $_SESSION["nombre"]; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
          
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          
            <!--====================================================== 
            INICIO
            ========================================================-->
            <li class="nav-item has-treeview menu-open">
                <a href="<?php echo $url ?>inicio" class="nav-link">
                    <i class="fas fa-home"></i>
                    <p>
                        Inicio                        
                    </p>
                </a>
            </li>
            
            <li class="nav-item has-treeview menu-open">
                <a href="#" class="nav-link">
                    <i class="fas fa-users"></i>
                    <p>
                      Usuarios
                      <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Active Page</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo $url ?>usuarios" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Usuarios Registrados</p>
                        </a>  
                    </li>
                </ul>
            </li>
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

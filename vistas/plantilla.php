<?php

    session_start();

?>

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <?php

            $url = Ruta::ctrRuta();

        ?>

      <title>Sistema De Inventarios</title>

      <!-- DataTables -->
      <link rel="stylesheet" href="<?php echo $url ?>vistas/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
      <link rel="stylesheet" href="<?php echo $url ?>vistas/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
      <!-- Font Awesome Icons -->
      <link rel="stylesheet" href="<?php echo $url ?>vistas/plugins/fontawesome-free/css/all.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="<?php echo $url ?>vistas/dist/css/adminlte.css">
      <link rel="stylesheet" href="<?php echo $url ?>vistas/dist/css/estilos.css">
      <!-- Google Font: Source Sans Pro -->
      <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

      <!-- SWEETALERT2 -->
      <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    </head>
    
    
        
    <body class="hold-transition sidebar-mini">
        
        
         <!--====================================================== 
         wrapper
         ========================================================-->
        
            
            <?php
            
            if(isset($_SESSION["iniciarSession"]) && $_SESSION["iniciarSession"] == "ok"){

                echo '<div class="wrapper">';
             
                 /*=============================================
                 CABEZOTE
                 =============================================*/
             
                 include "modulos/cabecera.php";
             
                 /*=============================================
                 MENU
                 =============================================*/
             
                 include "modulos/menu.php";
             
                 /*=============================================
                 CONTENIDO
                 =============================================*/
             
                 if(isset($_GET["ruta"])){
             
                   if($_GET["ruta"] == "inicio" ||
                      $_GET["ruta"] == "usuarios" ||
                      $_GET["ruta"] == "categorias" ||
                      $_GET["ruta"] == "productos" ||
                      $_GET["ruta"] == "clientes" ||
                      $_GET["ruta"] == "ventas" ||
                      $_GET["ruta"] == "crear-venta" ||
                      $_GET["ruta"] == "reportes" ||
                      $_GET["ruta"] == "salir"){
             
                     include "modulos/".$_GET["ruta"].".php";
             
                   }else{
             
                     include "modulos/404.php";
             
                   }
             
                 }else{
             
                   include "modulos/inicio.php";
             
                 }
             
                 /*=============================================
                 FOOTER
                 =============================================*/
             
                 include "modulos/footer.php";
             
             
               }else{
             
                 include "modulos/login.php";
             
               }
             
            
            ?>


    <!-- jQuery -->
    <script src="<?php echo $url ?>vistas/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo $url ?>vistas/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $url ?>vistas/dist/js/adminlte.min.js"></script>

    <!-- DataTables -->
    <script src="<?php echo $url ?>vistas/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $url ?>vistas/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo $url ?>vistas/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo $url ?>vistas/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>


    <!--=============================================
    ARCHIVOS JAVASCRIPT PERSONALIZADOS
    =============================================-->           

    <script src="<?php echo $url ?>vistas/js/usuarios.js"></script>

    <!-- page script -->
    <script>
      $(function () {
          $("#example1").DataTable({
            "responsive": true,
            "autoWidth": false,
          });
          
      });
      </script>
    
    </body>
</html>             
            
       
